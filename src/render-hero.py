##################
# GNOME.org Hero #
##################

# Run this script with `Alt+P` to render all assets into the website folders.

import bpy
import os

# List of scenes to render and their corresponding output filenames
scenes_to_render = [
    {"scene_name": "sharing card", "output_filename": "card.png"},    
    {"scene_name": "laptop", "output_filename": "laptop.webp"},
    {"scene_name": "laptop-dark", "output_filename": "laptop-dark.webp"},
#    {"scene_name": "download", "output_filename": "download.webp"},
#    {"scene_name": "download-dark", "output_filename": "download-dark.webp"}
]


# Set common render settings
#bpy.context.scene.render.engine = 'CYCLES'  # Change to 'BLENDER_EEVEE' if you prefer Eevee
#bpy.context.scene.render.resolution_x = 1920  # Set resolution
#bpy.context.scene.render.resolution_y = 1080

# Render each scene
for scene_info in scenes_to_render:
    # Set scene
    scene_name = scene_info["scene_name"]
    bpy.context.window.scene = bpy.data.scenes[scene_name]
    
    # Set output filename and format
    output_filename = scene_info["output_filename"]
    output_path = "//../img/" + output_filename
    file_format = os.path.splitext(output_filename)[1][1:].upper()  # Extract extension and convert to uppercase
    
    bpy.context.scene.render.filepath = output_path
    bpy.context.scene.render.image_settings.file_format = file_format
    
    # Render scene
    bpy.ops.render.render(write_still=True)
